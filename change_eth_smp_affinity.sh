#!/system/bin/sh

eth_irq=$(cat /proc/interrupts | grep eth0 | busybox awk '{print $1}')
eth_irq_num=${eth_irq:0:2}

echo 1 > /proc/irq/$eth_irq_num/smp_affinity_list
